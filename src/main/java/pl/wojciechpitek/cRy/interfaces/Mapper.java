package pl.wojciechpitek.cRy.interfaces;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

public interface Mapper<From, To> {

    To map(From from);

    default List<To> map(List<From> from) {
        return Optional
                .ofNullable(from)
                .orElse(new ArrayList<>())
                .stream()
                .filter(Objects::nonNull)
                .map(this::map)
                .collect(Collectors.toList());
    }
}
