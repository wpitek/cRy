package pl.wojciechpitek.cRy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.core.env.Environment;
import org.springframework.core.env.SimpleCommandLinePropertySource;

import javax.annotation.PostConstruct;
import java.util.Arrays;

@Slf4j
@SpringBootApplication
@EnableAutoConfiguration
@EntityScan(basePackages = "pl.wojciechpitek.cRy.entity")
public class CryApp extends SpringBootServletInitializer {

    @Autowired
    private Environment env;

    @PostConstruct
    public void onAppStartup() {
        if (env.getActiveProfiles().length == 0) {
            log.info("cRy app runs with default profile: dev");
        } else {
            log.info("cRy app runs with profiles: {}", Arrays.toString(env.getActiveProfiles()));
        }
    }

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(CryApp.class);
        SimpleCommandLinePropertySource source = new SimpleCommandLinePropertySource(args);
        addDefaultProfile(springApplication, source);
        springApplication.run(args);
    }

    private static void addDefaultProfile(SpringApplication app, SimpleCommandLinePropertySource source) {
        if (!source.containsProperty("spring.profiles.active")) {
            app.setAdditionalProfiles("dev");
        }
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(CryApp.class);
    }

}
