package pl.wojciechpitek.cRy.beans;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.io.PathResource;

import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Configuration
public class ProprtyConfigurationBean {

    @Bean
    public PropertySourcesPlaceholderConfigurer getSourcesPropertyPlaceholderConfigurer() {
        final PropertySourcesPlaceholderConfigurer configurer = new PropertySourcesPlaceholderConfigurer();
        Path LINUX_CONF_PATH = Paths.get("/etc", "cRy", "cRy.properties");
        if (LINUX_CONF_PATH.toFile().exists()) {
            configurer.setLocation(new PathResource(LINUX_CONF_PATH));
            log.info("Config location: {}", LINUX_CONF_PATH);
        } else {
            Path DEV_CONF_PATH = Paths.get(System.getenv("HOMEDRIVE"), "dev", "cRy.properties");
            configurer.setLocation(new PathResource(DEV_CONF_PATH));
            log.info("Config location: {}", DEV_CONF_PATH);
        }
        return configurer;
    }

}
