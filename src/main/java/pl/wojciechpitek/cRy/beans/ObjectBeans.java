package pl.wojciechpitek.cRy.beans;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

@Component
public class ObjectBeans {

    @Bean
    public ObjectMapper bObjectMapper() {
        return new ObjectMapper();
    }
}
